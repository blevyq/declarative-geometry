#|
  This file is a part of mathsvg project.
|#

(in-package :cl-user)
(defpackage geodecl-asd
  (:use :cl :asdf))
(in-package :geodecl-asd)

(defsystem geodecl
  :version "0.1"
  :author ""
  :license ""
  :depends-on (:iterate :alexandria :cl-svg :named-readtables :short-lambdas :anaphora :cl-generic-arithmetic :repl-utilities :cl-annot)
  :components ((:module "src"
                :components
                ((:file "readtable")
                 (:file "utils")
                 (:file "function")
                 (:file "picture")
                 (:file "svg"))))
  :description ""
  :long-description
  #.(with-open-file (stream (merge-pathnames
                             #p"README.markdown"
                             (or *load-pathname* *compile-file-pathname*))
                            :if-does-not-exist nil
                            :direction :input)
      (when stream
        (let ((seq (make-array (file-length stream)
                               :element-type 'character
                               :fill-pointer t)))
          (setf (fill-pointer seq) (read-sequence seq stream))
          seq)))
  :in-order-to ((test-op (test-op geodecl-test))))
