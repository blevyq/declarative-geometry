;; Motivation:
;; mathematicians write 1+f, where f is a function, to mean lambda x -> 1+f(x).
;; When coding mathematical diagrams, lisp could understand something like
;; (line 0 #'cis)
;; to mean draw a line from 0 to the point on the unit circle given by calling cis on some parameter.
;; This is the lisp analogue to a concise, natural notation for the problem domain.
;; But it has an added benefit: we can build tools for drawing stills and use them for animation by passing in functions of time instead of constants.

(defpackage geodecl.function
  (:use :cl/ga :iterate))
(in-package :geodecl.function)

(named-readtables:in-readtable geodecl.readtable:syntax)

;; These are extensible so things like splines, polynomials, and continuous functions can be represented with plain old data structures, but still called..
@export
(defmethod functionyp (fn)
  "An extensible does-this-look-like-a-function predicate."
  (functionp fn))

@export
(defmethod fwrap (thing)
  "Creates a function that assigns behavior to thing.
Examples:
If fn is a constant, (fwrap x) is the function that returns x.
If thing is already a function, (fwrap fn) = fn "
  (assert (not (functionyp thing)))
  (lambda (&rest args)
    (declare (ignore args))
    thing))
(defmethod fwrap ((fn function)) fn)

@export
(defun fapply (fn list)
  "Applies fn on list. If fn is not a function, we check to see if there is an associated behavior, and use that. If some of the arguments are functions, compose instead."
  (if (some #'functionyp list)
      (lambda (&rest args)
        (apply (fwrap fn)
               (mapcar [(fapply _0 args)] list)))
      (apply (fwrap fn) list)))
;; we will have to generarlize this to support user-defined functiony types

@export
(defun fcall (fn &rest args)
  "Calls fn on args. If fn is not a function, we check to see if there is an associated behavior, and use that. If some of the arguments are functions, compose instead.
Example:
(fcall + 1 #'identity)
returns
(lambda (x) (+ 1 x))"
  (fapply fn args))

@export
(defmacro defun-fcall (name arglist &body body)
  "Like defun, but uses fcall."
  (alexandria:with-gensyms (args)
    `(defun ,name (&rest ,args)
       (fapply (lambda ,arglist ,@body)
               ,args))))
;; fix slime arglists?
;; docstring?


;; CL (wisely?) locks the CL package. But we can still redefine mathematical operators to use fcall, thanks to cl/ga
;; nullary
(defmethod nullary-+ (f) 0)
(defmethod nullary-* (f) 1)
;; unary methods
(defmethod unary-+ (f) (fcall #'cl:+ f))
(defmethod unary-- (f) (fcall #'cl:- f))
(defmethod unary-* (f) (fcall #'cl:* f))
(defmethod unary-/ (f) (fcall #'cl:/ f))
;; binary methods
(defmethod binary-+ (f g) (fcall #'cl:+ f g))
(defmethod binary-- (f g) (fcall #'cl:- f g))
(defmethod binary-* (f g) (fcall #'cl:* f g))
(defmethod binary-/ (f g) (fcall #'cl:/ f g))
;; other good things
(defmacro unary-fcall/ga (&body fns)
  `(progn
     ,@(iter (for fn in fns)
         (collect
             `(defmethod ,fn (f)
                (fcall #',fn f))))))
(unary-fcall/ga exp cis cos sin tan cosh tanh sinh 1+ 1- sqrt realpart imagpart)

(defmethod log (number &optional (base (exp 1)))
  (fcall #'log number base))
(defmethod expt (base expt)
  (fcall #'expt base expt))
