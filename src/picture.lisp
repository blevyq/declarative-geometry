;; This file defines a dsl for defining pictures.
(defpackage geodecl.picture
  (:use :cl/ga :iterate :geodecl.function)
  (:import-from :repl-utilities :deflex)
  (:shadow :class :text))
(in-package :geodecl.picture)

(named-readtables:in-readtable geodecl.readtable:syntax)

@export
(deflex i #C(0 1) "The imaginary unit!")
@export
(deflex tau (* 2 pi) "The *true* circle constant")

(defvar *no-collect* (gensym "no collect"))
@export
(defvar *nodes* *no-collect*
  "A list of nodes to be gathered. Set to *no-collect* to turn off node gathering (default).")

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun keytern (name &optional (package :keyword))
    "Like `intern', but calls string on the argument and defaults to interning in the `keyword' package."
    (intern (string name) package))

  (defun proc-slot (slot)
    "Processes defshape slot definitions"
    (destructuring-bind (name &optional type &rest args) slot
      `(,name :initarg ,(keytern name)
              :type ',type
              :reader ,name
              ,@args))))

@export
(defmacro defshape (class supers pos-slots &body key-slots)
  (let ((pos-names (mapcar #'car pos-slots))
        (key-names (mapcar #'car key-slots))
        (make-class (intern (format nil "MAKE-~a" class))))
    `(progn
       (defclass ,class ,supers
         ,(mapcar #'proc-slot (append pos-slots key-slots)))

       (defmethod print-object ((obj ,class) s)
         (format s "(~a ~{~s~^ ~}~{ ~{:~s ~s~}~})" ',class
                 (list ,@(iter (for slot in pos-names)
                           (collect `(,slot obj))))
                 (list ,@(iter (for slot in key-names)
                           (collect `(list ',slot (,slot obj)))))))

       (export ',(append pos-names key-names))

       (defun-fcall ,make-class (,@pos-names &rest args &key &allow-other-keys)
         (apply #'make-instance
                ',class
                ,@(iter (for name in pos-names)
                    (appending `(,(keytern name) ,name)))
                args))

       (defun ,class (&rest args)
         (let ((shape (apply #',make-class args)))
           (unless (eq *nodes* *no-collect*) (push shape *nodes*))
           shape)))))



@export
(deftype point () 'number)
@export
(deftype nonnegative () '(real 0 *))

@export
(defshape node () ()
  (id string :initform nil)
  (visibility t :initform t)
  (class string :initform nil)
  (children list :initform nil))

@export
(defshape circle (node)
    ((center point)
     (radius nonnegative)))

@export
(defshape line (node)
    ((start point)
     (end point)))

@export
(defshape polyline (node)
    ((points sequence)))

@export
(defshape arc (node)
    ((center point)
     (radius nonnegative)
     (start real)
     (end real))
  (clockwise? boolean :initform nil))

@export
(defshape polygon (node) ((points)))

@export
(defshape path (node)
    ((d string)))

@export
(defshape label (node)
    ((anchor t)
     (text string))
  (dz number :initform 0))

@export
(defshape marker (node) ()
  (view-box sequence)
  (ref-pt point)
  (size point)
  (units t :initform nil)
  (orient string))

@export
(defun-fcall cjuxt (x y)
  (+ x (* i y)))

@export
(defvar *default-parametric-slices* 100)
@export
(defun parametric (fn &key (slices *default-parametric-slices*)
                        (dur 1))
  (polyline (iter (for k from 0 to slices)
              (collect (funcall fn (* (/ slices) k dur))))))

(deflex inf most-positive-double-float)
(defun-fcall clamp (val &key (min (- inf)) (max inf))
  (min (max min val) max))
@export
(defvar *default-interp-tdur* 1)
@export
(defun interp (start end &key (tstart 0) (tdur *default-interp-tdur* tdurp)
                           (tend (+ tstart tdur) tendp))
  "Interpolates between start and end over time interval tstart to tend."
  (assert (not (and tdurp tendp)))
  (let ((duration (- tend tstart)))
    (lambda (world-time)
      (let ((time (clamp (/ (- world-time tstart) duration) :min 0 :max 1)))
        (fcall (+ (* (- 1 time) start)
                  (* time end))
               world-time)))))
