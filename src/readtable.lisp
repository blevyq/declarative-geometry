(defpackage geodecl.readtable
  (:use :cl :named-readtables)
  (:export :syntax))
(in-package geodecl.readtable)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defreadtable annot-readtable
    (:merge nil)
    (:macro-char #\@ #'annot.syntax:annotation-syntax-reader)))

(in-package geodecl.readtable)

(defreadtable syntax
    (:merge short-lambdas:syntax annot-readtable))
