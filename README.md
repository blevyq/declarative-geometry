# What is it

This is a domain specific language in Common lisp for animated plotting to svgs.
The focus is on 2d mathematical animations.

# Why is it

## I had problems with asymptote

### paramaters are confusing

### animation was too low level

### language was unfamiliar

### hard to separate style from presentation

# How do I use it

See the examples folder and `example.lisp`!

# design principles + idiosyncrasies

## use mathematical tricks

### points are complex numbers

### `1 + f = lambda x -> 1 + f(x)`

## be extensible

## be functional + declarative

## separate content and presentation

## html5 ready

### use css to style

## use existing tools

## use syntactic sugar

### short-lambdas

One line lambda expressions are written like

    [(+ _0 _1)]

for

    (lambda (_0 _1) (+ _0 _1))

### cl/ga

### anaphora

### repl-utilities

Specifically for `deflex`.

### cl-annot
