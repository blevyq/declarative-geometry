#|
  This file is a part of mathsvg project.
|#

(in-package :cl-user)
(defpackage mathsvg-test-asd
  (:use :cl :asdf))
(in-package :mathsvg-test-asd)

(defsystem mathsvg-test
  :author ""
  :license ""
  :depends-on (:mathsvg
               :prove)
  :components ((:module "t"
                :components
                ((:test-file "mathsvg"))))

  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run-test-system) :prove-asdf) c)
                    (asdf:clear-system c)))
