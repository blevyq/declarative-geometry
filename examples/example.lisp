(ql:quickload 'geodecl)
(defpackage example
  (:use :cl/ga :iterate :geodecl.utils :geodecl.picture :geodecl.svg)
  (:shadowing-import-from :geodecl.svg :time)
  (:shadowing-import-from :geodecl.picture :class)
  (:import-from :repl-utilities :deflex))
(in-package :example)

(named-readtables:in-readtable geodecl.readtable:syntax)


;; --------------------------------------
;; euler's theorem
(defun factorial (n &optional (prod 1))
  "naive factorial algorithm"
  (if (< n 2) prod
      (factorial (1- n) (* n prod))))

(defun term-n (z n)
  "The n-th term in the taylor series of e^iz"
  (/ (expt (* i z) n)
     (factorial n)))

(defun sum-n (z n)
  "The sum of the first n terms in the taylor series of e^iz"
  (if (< n 0) 0
      (iter (for k from 0 below n)
        (summing (term-n z k)))))

(setf *css* (concatenate 'string geodecl.svg:*marker-sty* (slurp "style.css")))

(let ((terms 20))
  (spit-svg
   (with-svg (:view-box "-40 -40 80 80")
     (circle 0 1 :class '("axis"))
     (iter (for n from 0 to terms)
       (line (sum-n time (1- n))
             (sum-n time n))))
   "examples/taylor" :dur tau :framerate 30))

;; --------------------------------------
;; parametric test -- draw a spiral
(spit-svg
 (with-svg (:view-box "-4 -4 8 8")
   (parametric (* 1/8 #'identity (cis #'identity)) :dur 32 :slices 300))
 "examples/spiral")


;; --------------------------------------
;; derivative of circle
(progn
  (spit-svg
   (with-svg (:view-box "-2 -2 4 4")
     (circle 0 1)
     (label (line 0 #'cis :class '("end-arrow")) "z")
     (let ((*interp-tdur* (/ tau 4)))
       (if (< time *interp-tdur*)
           (line #'cis (* #C(1 1) #'cis) :class '("end-arrow" "axis")))893893
       (label (line (interp #'cis 0) (* (interp #C(1 1) i) #'cis)
                    :class '("end-arrow")) "z'")
       (if (> time (+ *interp-tdur* .5))
           (label (line (* i #'cis) (* #C(-1 1) #'cis) :class '("end-arrow"))
                  "z''"))))
   "examples/circle-derivatives" :dur  tau :frames 200)

  (uiop:with-current-directory ("examples/circle-derivatives/")
    (uiop:run-program "../../svganimator/svganimator.py -b -s .1 circle-derivatives.svg $(ls svg/* | sort -n)")))
